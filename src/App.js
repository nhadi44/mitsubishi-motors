import { Components } from "./components";
import styled from "styled-components";
import "./App.css";
import AppsBanner from "./assets/images/App.png";
import { useEffect } from "react";
import axios from "axios";
import { useState } from "react";
import iconLocation from "./assets/icons/current.png";

const Header = styled.header`
  background-color: #000000;
  color: #fff;
`;

const Container = styled.div`
  max-width: 1320px;
  margin: 0 auto;
  padding: ${(props) => props.padding || "0"};
  overflow: hidden;

  @media only screen and (max-width: 480px) {
    padding: 1em 2em;
  }
`;

const Row = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(300px, 1fr));
  grid-gap: 1em;
  margin-bottom: 2em;
`;

const Col = styled.div`
  width: 100%;
`;

const LoadMore = styled.button`
  background-color: #000;
  color: #fff;
  border: none;
  padding: 1rem;
  font-size: 16px;
  font-weight: 700;
  text-transform: uppercase;
  cursor: pointer;
`;

const Banner = styled.img`
  width: 100%;
`;

const CurrentLocation = styled.button`
  display: flex;
  align-items: center;
  margin: 0.5em 0;
  border: none;
  cursor: pointer;
`;
const CurrentLocationImage = styled.img`
  max-width: 100%;
`;

export const App = () => {
  const [nearestLocation, setNearestLocation] = useState([]);
  const [latitude, setLatitude] = useState("");
  const [longitude, setLongitude] = useState("");
  const [page, setPage] = useState(1);
  const [nextPage, setNextPage] = useState(9);
  const [permisionDenied, setPermisionDenied] = useState();
  const [loading, setLoading] = useState(false);
  const [locationByProvince, setLocationByProvince] = useState([]);

  const getNearestDealers = async () => {
    const baseUrl =
      "https://mitsubishi-50.sudahdistaging.in/api/frontend/search-dealers";

    await axios
      .get(baseUrl, {
        params: {
          page: page,
          limit: nextPage,
          latitude: latitude,
          longitude: longitude,
        },
      })
      .then((res) => {
        console.log(longitude, latitude);
        setNearestLocation(res.data.data);
      });
  };

  const getDealersByLocation = async (e) => {
    const baseUrl =
      "https://mitsubishi-50.sudahdistaging.in/api/frontend/search-dealers";

    await axios
      .get(baseUrl, {
        params: {
          page: page,
          limit: nextPage,
          keyword: e,
        },
      })
      .then((res) => {
        setLocationByProvince(res.data.data);
        setNearestLocation(res.data.data);
        setLoading(false);
      });
  };

  const getLocation = () => {
    navigator.geolocation.getCurrentPosition(
      function (position) {
        setLatitude(position.coords.latitude);
        setLongitude(position.coords.longitude);
        setPermisionDenied(false);
      },
      function (error) {
        setPermisionDenied(error.PERMISSION_DENIED);
      }
    );
  };

  useEffect(() => {
    getNearestDealers();
    getLocation();
    getDealersByLocation();
  }, []);

  return (
    <>
      <Header>
        <Components.Navbar />
      </Header>
      <Components.Hero />
      <main style={{ position: "relative" }}>
        <Container padding="6rem 9rem">
          <Components.HeaderDescription />
          <Components.Search getData={getDealersByLocation} />
          {!permisionDenied ? (
            ""
          ) : (
            <CurrentLocation onClick={getLocation}>
              <CurrentLocationImage src={iconLocation} /> Gunakan lokasi saya
              saat ini
            </CurrentLocation>
          )}
        </Container>
        {!permisionDenied ? (
          <>
            <Container padding="1rem 6rem">
              <Row>
                {nearestLocation?.slice(0, nextPage)?.map((item) => (
                  <Col key={item.id}>
                    <Components.Card
                      id={item.id}
                      header={item.title}
                      address={item.address}
                      service={item.services}
                    />
                  </Col>
                ))}
              </Row>
            </Container>
            <Container
              style={{
                display: "flex",
                justifyContent: "center",
                marginBottom: "2em",
              }}
            >
              <LoadMore
                onClick={() => {
                  setPage(page + 1);
                  setNextPage(nextPage + 9);
                  setLoading(true);
                  getNearestDealers();
                }}
              >
                {loading ? "Loading..." : "Load More"}
              </LoadMore>
            </Container>
          </>
        ) : (
          <>
            <Container padding="1rem 6rem">
              <Row>
                {locationByProvince?.slice(0, nextPage)?.map((item) => (
                  <Col key={item.id}>
                    <Components.Card
                      header={item.title}
                      address={item.address}
                      service={item.services}
                      bengkelOperational={item.bengkel_operational_hours}
                      showroomOperational={item.showroom_operational_hours}
                      phone={item.phone}
                      longitude={item.longitude}
                      latitude={item.latitude}
                    />
                  </Col>
                ))}
              </Row>
            </Container>
            <Container
              style={{
                display: "flex",
                justifyContent: "center",
                marginBottom: "2em",
              }}
            >
              <LoadMore
                onClick={() => {
                  setPage(page + 1);
                  setNextPage(nextPage + 9);
                  setLoading(true);
                  getDealersByLocation();
                }}
              >
                {loading ? "Loading..." : "Load More"}
              </LoadMore>
            </Container>
          </>
        )}

        <Container>
          <Banner src={AppsBanner} />
        </Container>
        <Container padding="1rem 6rem" style={{ marginBottom: "7em" }}>
          <Components.About />
        </Container>
        <Components.Footers />
      </main>
    </>
  );
};
