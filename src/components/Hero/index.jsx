import HeroImage from "../../assets/images/Hero.png";
import styled from "styled-components";

const Image = styled.img`
  width: 100%;
`;

export const Hero = () => {
  return (
    <>
      <Image src={HeroImage} alt="Hero" />
    </>
  );
};
