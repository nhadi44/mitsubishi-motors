import {
  Image,
  Nav,
  NavbarItem,
  NavbarLink,
  NavbarBrand,
  NavbarSubItem,
  NavbarWrapper,
  NavbarLinkMenu,
} from "./styled";
import Logo from "../../assets/images/Logo.png";
import Euro from "../../assets/images/Euro.png";
import { Chevron } from "../Icons/Chevron";
import { Menu } from "../Icons/Menu";

export const Navbar = () => {
  return (
    <>
      <Nav>
        <NavbarBrand>
          <Image src={Logo} alt="logo" />
        </NavbarBrand>

        <NavbarWrapper>
          <NavbarLink>
            <Image src={Euro} alt="euro" />
          </NavbarLink>

          <NavbarItem>
            <NavbarSubItem>
              <NavbarLinkMenu>
                About Us <Chevron color={"#FAFAFA"} />
              </NavbarLinkMenu>
            </NavbarSubItem>

            <NavbarSubItem>
              <NavbarLinkMenu>
                About Us <Chevron color={"#FAFAFA"} />
              </NavbarLinkMenu>
            </NavbarSubItem>

            <NavbarSubItem>
              <NavbarLinkMenu>Promo</NavbarLinkMenu>
            </NavbarSubItem>

            <NavbarSubItem>
              <NavbarLinkMenu>
                News & Events <Chevron color={"#FAFAFA"} />
              </NavbarLinkMenu>
            </NavbarSubItem>
          </NavbarItem>

          <NavbarLink>
            <Menu />
          </NavbarLink>
        </NavbarWrapper>
      </Nav>
    </>
  );
};
