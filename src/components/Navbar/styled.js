import styled from "styled-components";

export const Nav = styled.nav`
  color: #fff;
  height: 80px;
  max-width: 1440px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 0 20px;
  background-color: #000;
  margin: 0 auto;
`;

export const Image = styled.img`
  max-width: 100%;
`;

export const NavbarWrapper = styled.div`
  display: flex;
  align-items: center;
  gap: 2rem;
`;

export const NavbarBrand = styled.a`
  text-decoration: none;
`;

export const NavbarLink = styled.a`
  text-decoration: none;
`;
export const NavbarLinkMenu = styled.a`
  text-decoration: none;
  display: flex;
  align-items: center;
  justify-content: center;
  gap: 0.8rem;
`;

export const NavbarItem = styled.ul`
  list-style: none;
  display: flex;
  align-items: center;
  gap: 2.5rem;

  @media only screen and (max-width: 992px) {
    display: none;
  }
`;

export const NavbarSubItem = styled.li`
  font-size: 16px;
  font-weight: 700;
  text-transform: uppercase;
`;
