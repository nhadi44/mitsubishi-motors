export const Chevron = ({ color }) => {
  return (
    <svg
      width="12"
      height="7"
      viewBox="0 0 12 7"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M1.175 0.158325L5 3.97499L8.825 0.158325L10 1.33332L5 6.33332L0 1.33332L1.175 0.158325Z"
        fill={color}
      />
      <path
        d="M1.175 0.158325L5 3.97499L8.825 0.158325L10 1.33332L5 6.33332L0 1.33332L1.175 0.158325Z"
        fill="url(#paint0_linear_438_524)"
      />
    </svg>
  );
};
