import styled from "styled-components";

export const ModalWrapper = styled.div`
  position: fixed;
  left: 0;
  top: 0;
  right: 0;
  bottom: 0;
  background-color: rgba(0, 0, 0, 0.5);
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const ModalContent = styled.div`
  position: relative;
  background-color: #fff;
  width: 924px;
  height: 655.12px;
  padding: 1.5rem;
  
`;

export const ModalHeader = styled.div`
  padding: 10px;
`;

export const ModalTitle = styled.h4`
  margin: 0;
  font-size: 24px;
  font-weight: 700;
  text-transform: capitalize;
  margin-bottom: 0.5rem;
`;

export const ModalAddress = styled.p`
  font-size: 16px;
  font-weight: 400;
  color: #4c4c4c;
  margin-bottom: 0.6rem;
`;

export const ModalBody = styled.div`
  padding: 10px;
  display: grid;
  grid-template-columns: 0.1fr 0.9fr;
`;

export const ModalIcon = styled.img`
  max-width: 100%;
`;

export const ModalSection = styled.div``;
export const ModalService = styled.div`
  display: flex;
  gap: 0.5rem;
`;

export const ServiceName = styled.span`
  display: flex;
  align-items: center;
  gap: 0.5rem;
  color: #4c4c4c;
  font-size: 12px;
  text-transform: uppercase;
  font-weight: 500;
  margin-bottom: 0.8rem;
`;
export const Dot = styled.div`
  width: 0.3em;
  height: 0.3em;
  background-color: #4c4c4c;
  border-radius: 100%;
`;

export const ButtonClose = styled.button`
  position: absolute;
  right: 2%;
  top: 2.4%;
  transform: translate(-100%, 100%);
  border: none;
  font-size: 1.5rem;
  background-color: transparent;
  cursor: pointer;
`;

export const ViewDirection = styled.button`
  border: none;
  border-bottom: 2px solid #000;
  background-color: transparent;
  padding: 0.5rem 0;
  cursor: pointer;
  display: flex;
  align-items: center;
  gap: 0.5rem;
  font-size: 14px;
  text-transform: uppercase;
  font-weight: 700;
  margin-bottom: 1.5rem;
`;

export const MapViewDirection = styled.img`
  max-width: 100%;
`;

export const ButtonGroup = styled.div`
  display: flex;
  gap: 1rem;
`;
export const ButtonTestDrive = styled.button`
  border: none;
  background-color: #000;
  color: #fff;
  font-size: 16px;
  font-weight: 700;
  text-transform: uppercase;
  padding: 1rem 1.2rem;
`;
export const ButtonBook = styled.button`
  border: 2px solid #000;
  background-color: #fff;
  color: #000;
  font-size: 16px;
  font-weight: 700;
  text-transform: uppercase;
  padding: 1rem 1.2rem;
`;

export const ModalSchedule = styled.div`
  margin: 3rem 0;
  display: grid;
  grid-template-columns: 1fr 1fr;
`;
export const ShowrommSchedule = styled.div``;
export const BengkelSchedule = styled.div``;
export const ScheduleHeader = styled.h4`
  padding: 0;
  font-size: 20px;
  font-weight: 500;
  text-transform: capitalize;
  margin-bottom: 0.8rem;
  color: #4c4c4c;
`;
export const ScheduleBody = styled.div`
  display: flex;
  flex-direction: column;
  gap: 1rem;
`;
export const ScheduleItem = styled.div`
  display: flex;
  gap: 1rem;
`;
export const ScheduleDay = styled.span`
  font-size: 16px;
  font-weight: 400;
`;
export const ScheduleTime = styled.span`
  font-size: 16px;
  font-weight: 400;
`;

export const ModalContact = styled.div``;
export const ModalContactHeader = styled.h4`
  padding: 0;
  font-size: 20px;
  font-weight: 500;
  text-transform: capitalize;
  margin-bottom: 0.8rem;
  color: #4c4c4c;
`;

export const ModalContactBody = styled.div``;
export const ModalContactItem = styled.div`
  display: grid;
  grid-template-columns: 0.05fr 0.9fr;
  margin-bottom: 1rem;
`;
export const ModalContactPhone = styled.span`
  display: flex;
  flex-direction: column;
  gap: 0.5rem;
`;
export const ModalContactEmail = styled.span``;
