import {
  BengkelSchedule,
  ButtonBook,
  ButtonClose,
  ButtonGroup,
  ButtonTestDrive,
  Dot,
  MapViewDirection,
  ModalAddress,
  ModalBody,
  ModalContact,
  ModalContactBody,
  ModalContactHeader,
  ModalContactItem,
  ModalContactPhone,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalIcon,
  ModalSchedule,
  ModalSection,
  ModalService,
  ModalTitle,
  ModalWrapper,
  ScheduleBody,
  ScheduleDay,
  ScheduleHeader,
  ScheduleItem,
  ScheduleTime,
  ServiceName,
  ShowrommSchedule,
  ViewDirection,
} from "./styled";

import Pin from "../../assets/icons/pin-red.png";
import Map from "../../assets/icons/map.png";
import Phone from "../../assets/icons/phone.png";
import Email from "../../assets/icons/mailModal.png";
import { Button } from "../Search/styled";

export const Modal = (props) => {
  if (!props.show) {
    return null;
  }

  const handleViewDirection = () => {
    window.open(
      `https://www.google.com/maps/dir/?api=1&destination=${props.latitude},${props.longitude}`,
      "_blank"
    );
  };

  return (
    <>
      <ModalWrapper onClick={props.onClose}>
        <ModalContent onClick={(e) => e.stopPropagation()}>
          <ButtonClose onClick={props.onClose}>X</ButtonClose>
          <ModalBody>
            <ModalIcon src={Pin} alt="modal-icon" />
            <ModalSection>
              <ModalService>
                {props.services.map((item, index) => (
                  <ServiceName key={index}>
                    {item} {index < props.services.length - 1 && <Dot />}
                  </ServiceName>
                ))}
              </ModalService>
              <ModalTitle>{props.header}</ModalTitle>
              <ModalAddress>{props.address}</ModalAddress>
              <ViewDirection onClick={handleViewDirection}>
                <MapViewDirection src={Map} /> view direction
              </ViewDirection>
              <ButtonGroup>
                <ButtonTestDrive>request test drive</ButtonTestDrive>
                <ButtonBook>Book Service</ButtonBook>
              </ButtonGroup>
              <ModalSchedule>
                <ShowrommSchedule>
                  <ScheduleHeader>showroom</ScheduleHeader>
                  <ScheduleBody>
                    {props.showroomOperational.map((item, index) => (
                      <ScheduleItem key={index}>
                        <ScheduleDay>{item.days}</ScheduleDay>
                        <ScheduleTime>{item.hours}</ScheduleTime>
                      </ScheduleItem>
                    ))}
                  </ScheduleBody>
                </ShowrommSchedule>
                <BengkelSchedule>
                  <ScheduleHeader>bengkel</ScheduleHeader>
                  <ScheduleBody>
                    {props.bengkelOperational.map((item, index) => (
                      <ScheduleItem key={index}>
                        <ScheduleDay>{item.days}</ScheduleDay>
                        <ScheduleTime>{item.hours}</ScheduleTime>
                      </ScheduleItem>
                    ))}
                  </ScheduleBody>
                </BengkelSchedule>
              </ModalSchedule>
              <ModalContact>
                <ModalContactHeader>Contact</ModalContactHeader>
                <ModalContactBody>
                  <ModalContactItem>
                    <ModalIcon src={Phone} alt="modal-icon" />
                    <ModalContactPhone>
                      <span>{props.phone}</span>
                    </ModalContactPhone>
                  </ModalContactItem>
                  <ModalContactItem>
                    <ModalIcon src={Email} alt="modal-icon" />
                    <ModalContactPhone>
                      <span>srikandimampang@gmail.com</span>
                    </ModalContactPhone>
                  </ModalContactItem>
                </ModalContactBody>
              </ModalContact>
            </ModalSection>
          </ModalBody>
        </ModalContent>
      </ModalWrapper>
    </>
  );
};
