import styled from "styled-components";

export const DescriptionWrapper = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 4em;

  @media only screen and (max-width: 480px) {
    flex-direction: column;
    margin-bottom: 2em;
  }
`;

export const H1 = styled.h1`
  flex: 50%;
  font-size: 48px;
  font-weight: 700;
  text-transform: uppercase;

  @media only screen and (max-width: 480px) {
    font-size: 32px;
    margin-bottom: 1em;
  }
`;

export const Description = styled.p`
  flex: 50%;
  font-size: 16px;
  font-weight: 400;

  @media only screen and (max-width: 480px) {
    font-size: 12px;
    padding: 0;
    text-align: justify;
  }
`;
