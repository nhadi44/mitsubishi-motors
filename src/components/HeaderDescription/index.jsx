import { Description, DescriptionWrapper, H1 } from "./styled";

export const HeaderDescription = () => {
  return (
    <>
      <DescriptionWrapper>
        <H1>Find Dealer</H1>
        <Description>
          Cari dan kunjungi dealer resmi Mitsubishi terdekat di kota Anda untuk
          mendapatkan pelayanan terbaik terkait dengan kendaraan dari Mitsubishi
          Motors Indonesia.
        </Description>
      </DescriptionWrapper>
    </>
  );
};
