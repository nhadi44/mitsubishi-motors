import { Navbar } from "./Navbar";
import { Hero } from "./Hero";
import { HeaderDescription } from "./HeaderDescription";
import { Search } from "./Search";
import { Card } from "./Card";
import { About } from "./About";
import { Footers } from "./Footer";
import { Modal } from "./Modal";

export const Components = {
  Navbar,
  Hero,
  HeaderDescription,
  Search,
  Card,
  About,
  Footers,
  Modal,
};
