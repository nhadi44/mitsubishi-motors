import styled from "styled-components";

export const Footer = styled.footer`
  margin-bottom: 14em;
  display: flex;
  justify-content: center;
  position: relative;
`;

export const FooterContainer = styled.div`
  width: 95%;
  height: 3.75em;
  background-color: #000;
  color: #fff;
  border-radius: 40px 0 0 40px;
  padding: 0 5em;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export const FooterContent = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 97%;
`;

export const FooterLinks = styled.div`
  display: flex;
  align-items: center;
  gap: 1em;
`;

export const FooterLinkName = styled.span`
  font-size: 14px;
  font-weight: 400;
  text-transform: uppercase;
`;

export const MiraWrapper = styled.div`
  background-color: #ba160a;
  width: 3%;
  padding: 0.5em;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const MiraButton = styled.button`
  width: 1.75em;
  height: 1.75em;
  border: 3px solid rgba(255, 255, 255, 0.5);
  background-color: transparent;
  box-shadow: 0 4px 4px rgba(0, 0, 0, 0.25);
  color: #fff;
`;

export const Icon = styled.img`
  max-width: 100%;
`;

export const AskMira = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  color: #fff;
  background-color: #ba160a;
  transform: translate(-10%, -200%);
  z-index: 1;
  display: flex;
  gap: 0.5em;
  align-items: center;
  border-radius: 13em;
  width: 11em;
  height: 3.75em;
  padding: 1em 1.5em;
`;

export const AskMiraButton = styled.button`
  border: none;
  background-color: transparent;
  color: #fff;
  font-weight: 600;
`;

export const AskMiraText = styled.span`
  font-size: 14px;
  font-weight: 700;
`;

export const MiraImage = styled.img`
  position: absolute;
  right: 0;
  height: auto;
  border-radius: 50%;
`;
