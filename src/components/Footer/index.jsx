import React from "react";
import Pin from "../../assets/icons/Pin.png";
import Download from "../../assets/icons/Download.png";
import Car from "../../assets/icons/Car.png";
import Calculator from "../../assets/icons/Calculator.png";
import Purchase from "../../assets/icons/Purchase.png";
import MiraImages from "../../assets/images/Mira.svg";

import {
  Footer,
  FooterContainer,
  FooterContent,
  FooterLinkName,
  FooterLinks,
  Icon,
  MiraButton,
  MiraWrapper,
  AskMira,
  MiraImage,
  AskMiraButton,
  AskMiraText,
} from "./styled";

export const Footers = () => {
  return (
    <>
      <Footer>
        <FooterContainer>
          <FooterContent>
            <FooterLinks>
              <Icon src={Pin} alt="footer-icon" />
              <FooterLinkName>find dealer</FooterLinkName>
            </FooterLinks>
            <FooterLinks>
              <Icon src={Download} alt="footer-icon" />
              <FooterLinkName>brochure download</FooterLinkName>
            </FooterLinks>
            <FooterLinks>
              <Icon src={Car} alt="footer-icon" />
              <FooterLinkName>Test drive</FooterLinkName>
            </FooterLinks>
            <FooterLinks>
              <Icon src={Calculator} alt="footer-icon" />
              <FooterLinkName>credit simulation</FooterLinkName>
            </FooterLinks>
            <FooterLinks>
              <Icon src={Purchase} alt="footer-icon" />
              <FooterLinkName>Purchase Consultation</FooterLinkName>
            </FooterLinks>
          </FooterContent>
        </FooterContainer>
        <MiraWrapper>
          <MiraButton>X</MiraButton>
        </MiraWrapper>
        <AskMira>
          <AskMiraButton>X</AskMiraButton>
          <AskMiraText>Ask MIRA</AskMiraText>
          <MiraImage src={MiraImages} alt="mira-image" />
        </AskMira>
      </Footer>
    </>
  );
};
