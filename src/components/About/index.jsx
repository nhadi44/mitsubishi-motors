import React from "react";
import {
  Sperator,
  StayConnected,
  Wrapper,
  Icon,
  SocialMedia,
  ContactUs,
  CopyRight,
  CompanyName,
} from "./styled";
import Facebook from "../../assets/icons/facebook.png";
import Twitter from "../../assets/icons/twitter.png";
import Instagram from "../../assets/icons/instagram.png";
import Youtube from "../../assets/icons/youtube.png";
import Mail from "../../assets/icons/mail.png";

export const About = () => {
  return (
    <>
      <Wrapper>
        <Sperator />
        <StayConnected>stay connected with us</StayConnected>
        <SocialMedia>
          <Icon src={Facebook} alt="social-media" />
          <Icon src={Twitter} alt="social-media" />
          <Icon src={Instagram} alt="social-media" />
          <Icon src={Youtube} alt="social-media" />
          <Icon src={Mail} alt="social-media" />
        </SocialMedia>
        <ContactUs>Contact Us</ContactUs>
        <Sperator width="3.5em" />
        <CopyRight>copyright © 2020.</CopyRight>
        <CompanyName>pt mitsubishi motors krama yudha sales indonesia</CompanyName>
      </Wrapper>
    </>
  );
};
