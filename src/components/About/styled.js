import styled from "styled-components";

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  gap: 1.5em;
  margin: 2em 0;
`;

export const Sperator = styled.div`
  width: ${(props) => props.width || "5em"};
  height: 0.2em;
  background: linear-gradient(212.58deg, #ff4d00 -29.35%, #da291c 90.64%);
  border-radius: 4px;
`;

export const StayConnected = styled.h5`
  font-size: 14px;
  font-weight: 700;
  text-transform: uppercase;
  color: #4c4c4c;
`;

export const SocialMedia = styled.div`
  display: flex;
  align-items: center;
  gap: 2em;
`;

export const Icon = styled.img`
  max-width: 100%;
`;

export const ContactUs = styled.h5`
  font-size: 14px;
  font-weight: 400;
  color: #0a0a0a;
`;

export const CopyRight = styled.h5`
  font-size: 14px;
  font-weight: 500;
  text-transform: uppercase;
  color: #4c4c4c;
`;

export const CompanyName = styled.h5`
  font-size: 14px;
  font-weight: 500;
  text-transform: uppercase;
  color: #4c4c4c;
`;
