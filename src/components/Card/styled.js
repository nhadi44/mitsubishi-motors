import styled from "styled-components";

export const CardWrapper = styled.div``;

export const CardBody = styled.div`
  display: grid;
  grid-template-columns: auto auto;
  grid-gap: 0.8rem;
  width: 100%;
  height: 14em;
  background-color: #f1f1f1;
  padding: 1.2rem;
  border-radius: 1.25rem;
  cursor: pointer;
`;

export const Pin = styled.img`
  max-width: 100%;
`;

export const Location = styled.div``;

export const LocationHeader = styled.h4`
  font-size: 1.25rem;
  font-weight: 700;
  margin-bottom: 1rem;
`;

export const LocationAddress = styled.p`
  font-size: 16px;
  font-weight: 400;
  color: #4c4c4c;
  margin-bottom: 4rem;
`;

export const LocationService = styled.div`
  display: flex;
  gap: 0.5rem;
  align-items: center;
`;

export const ServiceName = styled.span`
  display: flex;
  align-items: center;
  gap: 0.5rem;
  color: #4c4c4c;
  font-size: 12px;
  text-transform: uppercase;
  font-weight: 500;
`;

export const Dot = styled.div`
  width: 0.3em;
  height: 0.3em;
  background-color: #4c4c4c;
  border-radius: 50%;
`;
