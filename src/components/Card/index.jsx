import {
  CardBody,
  CardWrapper,
  Pin,
  Location,
  LocationHeader,
  LocationAddress,
  LocationService,
  Dot,
  ServiceName,
} from "./styled";
import PinImage from "../../assets/images/Pin.png";
import { Modal } from "../Modal";
import { useState } from "react";

export const Card = ({
  header,
  address,
  service,
  id,
  bengkelOperational,
  showroomOperational,
  phone,
  longitude,
  latitude,
}) => {
  const [show, setShow] = useState(false);

  const handleOpenModal = () => {
    setShow(true);
  };

  return (
    <>
      <CardWrapper>
        <CardBody onClick={handleOpenModal}>
          <Pin src={PinImage} alt="pin-icon" />
          <Location>
            <LocationHeader>{header}</LocationHeader>
            <LocationAddress>{address}</LocationAddress>
            <LocationService>
              {service.map((item, index) => (
                <ServiceName key={index}>
                  {item} {index < service.length - 1 && <Dot />}
                </ServiceName>
              ))}
            </LocationService>
          </Location>
        </CardBody>
      </CardWrapper>
      <Modal
        onClose={() => setShow(false)}
        show={show}
        address={address}
        header={header}
        services={service}
        bengkelOperational={bengkelOperational}
        showroomOperational={showroomOperational}
        phone={phone}
        longitude={longitude}
        latitude={latitude}
      />
    </>
  );
};
