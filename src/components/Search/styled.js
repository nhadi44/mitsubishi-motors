import styled from "styled-components";

export const SearchTitle = styled.h1`
  font-size: 20px;
  color: #9b9b9b;
  margin-bottom: 0.7em;
  @media only screen and (max-width: 480px) {
    font-size: 12px;
  }
`;

export const Wrapper = styled.div`
  /* position: relative; */
`;

export const Button = styled.button`
  display: flex;
  align-items: center;
  justify-content: space-between;
  border: 1px solid #9b9b9b;
  width: 50%;
  height: 40px;
  cursor: pointer;
  padding: 0 1em;
  background-color: #fff;

  @media only screen and (max-width: 480px) {
    width: 100%;
  }
`;

export const ButtonGroup = styled.div`
  display: flex;
  gap: 1em;
`;

export const DropdownBox = styled.ul`
  position: absolute;
  display: block;
  width: 30.09%;
  border-top: none;
  border-right: 1px solid #9b9b9b;
  border-left: 1px solid #9b9b9b;
  transition: all 0.3s ease-in-out;
  list-style: none;
  z-index: 100;
  background-color: #fff;

  @media only screen and (max-width: 480px) {
    width: 100%;
  }
`;

export const DropdownBoxItem = styled.li`
  width: 100%;
  padding: 0.5em 3.5em;
  border-bottom: 1px solid #9b9b9b;
  font-size: 12px;
  cursor: pointer;
  &:hover {
    background-color: #9b9b9b;
  }
`;
