import {
  Button,
  ButtonGroup,
  DropdownBox,
  DropdownBoxItem,
  SearchTitle,
  Wrapper,
} from "./styled";
import { SearchIcon } from "../Icons/Search";
import { Chevron } from "../Icons/Chevron";
import { useState } from "react";
import axios from "axios";
import { useEffect } from "react";
import { useRef } from "react";

export const Search = (props) => {
  const [showDropdown, setShowDropdown] = useState(false);
  const [provinces, setProvinces] = useState([]);
  const [selectProvince, setSelectProvince] = useState("Pilih Lokasi Terdekat");

  const getProvinces = async () => {
    const baseUrl =
      "https://mitsubishi-50.sudahdistaging.in/api/frontend/get-provinces";
    const response = await axios
      .get(baseUrl)
      .then((res) => res.data)
      .then((data) => {
        setProvinces(data);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  useEffect(() => {
    getProvinces();
  }, []);

  return (
    <>
      <SearchTitle>Discover the nearest dealership in your area</SearchTitle>
      <Wrapper>
        <Button onClick={() => setShowDropdown(!showDropdown)}>
          <ButtonGroup>
            <SearchIcon />
            <span>{selectProvince}</span>
          </ButtonGroup>
          <Chevron color={"#000"} />
        </Button>

        {showDropdown && (
          <DropdownBox>
            {provinces.map((province, index) => (
              <DropdownBoxItem
                key={index}
                onClick={() => {
                  setSelectProvince((prov) => (prov = province.name));
                  setShowDropdown(false);
                  props.getData(province.name);
                }}
              >
                <span>{province.name}</span>
              </DropdownBoxItem>
            ))}
          </DropdownBox>
        )}
      </Wrapper>
    </>
  );
};
